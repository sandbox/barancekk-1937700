/**
 * @file
 *  Javascript counter for site_maintenance module
 */

// Set 500ms Interval with callback of function that will count
// the remaining minutes and seconds and displays them. After
// the time is up interval is destroyed.
jQuery(document).ready(function () {
  var interval = setInterval(function(){
    var html_id = Drupal.settings.site_maintenance.html_id | '',
        activate_time = new Date(Drupal.settings.site_maintenance.activate_time * 1000),
        diff = activate_time - new Date(),
        minutes = Math.floor(diff / (1000 * 60)),
        seconds = Math.floor((diff - (minutes * 60 * 1000)) / 1000);

    if (diff < 0) {
      clearInterval(interval);
      minutes = seconds = 0;
    }

    if (html_id !== '') {
      jQuery('#' + html_id).text(minutes + Drupal.t('m') + ' ' + seconds + Drupal.t('s'));
    }
  },500);
});
