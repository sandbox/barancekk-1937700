CONTENTS OF THIS FILE
---------------------

 * About Site Maintenance
 * Force Login
 * Install
 * Credits

ABOUT SITE MAINTENANCE
----------------------

The aim of this module is to provide functionality to prevent users to login to
a page and displays them a message with countdown that the site is scheduled for
the maintenance and will be turned off. This gives the site users ability to
safely finish and save their work.

This module will disable following forms:
 - Disable user login
 - Disable user registration form
 - Disable user change password form

The module provides a way for site administrators and everyone with permission
to force the login. Simply add "?forceLogin" to /user/login url and if you are
site admin or have permission you will be allowed to log in.

FORCE LOGIN
-----------

When you want to login to site when the Site Maintenance module is enabled add
"?forceLogin" to /user/login url [http://yoursitename/user/login?forceLogin].
This module comes with permission site_maintenance_force_login that enables
force login.

INSTALL
-------

Install and enable the Site Maintenance module. This module is not dependent on
other modules.
Learn more about installing Drupal modules:
http://drupal.org/documentation/install/modules-themes/modules-7

CREDITS
-------

Site Maintenance was crafted by barancekk and lliss.
